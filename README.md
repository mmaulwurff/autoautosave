# Autoautosave

GZDoom mod: universal autosaver.

It works as a configurable checkpoint system.
It is designed to be compatible with any monster and weapon mod, and any map.

# Autosave on certain events:
- All enemies are eliminated
- Boss is eliminated
- No more active enemies (group kill)
- New active enemies (group alert)
- Boss is alerted
- All items are found
- Time has passed
- You are moved to another place
- Health drops low
- Health rises high
- Armor drops low
- Armor rises high
- Secret is found
- Powerup is found
- Weapon is found
- Key is found
- Backpack is found
- New armor type
- Major healing

# Thanks to
insightguy, Someone64, 4thcharacter, Tesculpture, SiFi270, ShockwaveS08, Rowsol,
Zhs2, dawnbreez, namsan, Crimsondrgn, Captain J, and Spaceman333 for help,
suggestions and bug reports.

Author: m8f

License: GPLv3 (see copying.txt)
